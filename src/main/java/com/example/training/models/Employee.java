package com.example.training.models;


import lombok.NoArgsConstructor;
import lombok.Data;
import lombok.AllArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Employee {
    public String id;
    public String name;
    public String address;
    public String phone;
    public String email;

}
