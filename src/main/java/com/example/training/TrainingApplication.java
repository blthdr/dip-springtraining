package com.example.training;

import com.example.training.models.Employee;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class TrainingApplication {

	public static void main(String[] args) {

		SpringApplication.run(TrainingApplication.class, args);

		// Instans Object Model
		Employee dataEmployee = new Employee();

		//Set Attribute Field Value
		dataEmployee.setName("Ivan");
		dataEmployee.setAddress("Bandung");
		dataEmployee.setEmail("Gmail");
		dataEmployee.setId("17");
		dataEmployee.setPhone("HP");

		//Get Attribute Field Value
		String employeeName = dataEmployee.getName();
		String employeeAddres = dataEmployee.getAddress();
		String employeeEmail = dataEmployee.getEmail();
		String employeeId = dataEmployee.getId();
		String employeePhone = dataEmployee.getPhone();

		// Show Value
		System.out.println("Employee Information");
		System.out.println("Employee Name :" + employeeName);
		System.out.println("Employee Address :" + employeeAddres);
		System.out.println("Employee Email :" + employeeEmail);
		System.out.println("Employee Id :" + employeeId);
		System.out.println("Employee Phone Number :" + employeePhone);
	}

}
