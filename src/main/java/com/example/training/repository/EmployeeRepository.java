package com.example.training.repository;

import com.example.training.models.Employee;
import com.example.training.viewmodel.EmployeeAddress;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public class EmployeeRepository {

    public List<Employee> findAllEmployee(){
        return null;
    }

    public List<Employee> findById(String idEmployee){
        return null;
    }

    public Integer insertEmployeeAddress(EmployeeAddress dataForSave){
        // Luas Bangunan
        // Warna Rumah
        // Jumlah Mobil
        // Ingin menggunakan model Employee tapi ditambah Atribute diatas yang tidak ada di model Employee
        // gunakan viewmodel

        dataForSave.setWarnaRumah("Merah");

        return null;
    }

    public Integer insertNewEmployee(Employee dataForSave){

        UUID uuid = UUID.randomUUID();
        dataForSave.setName("Nama");
        dataForSave.setPhone("Asd");

        return null;
    }

    public Integer deleteEmployee(String idEmployee){
        return null;
    }

    public Integer deleteAllEmployee(){
        return null;
    }
}
